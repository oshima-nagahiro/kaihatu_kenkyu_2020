<!DOCTYPE html>
<html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content="width=device-width", initial-scale="1">
      <title>第三回課題、指定行、列数</title>
    </head>
    <body>
      <h1>第三回課題、指定行、列数</h1>
      <form method="GET" action='loop2.php'>
          <input type="text" name="cols">行×
          <input type="text" name="colspan">列<br>
          <input type="submit" value="送信">
          <input type=reset value=" リセット ">
      </form>
      <hr>
      <table border="1" style="border-collapse: collapse">
          <?php
          for($i=0; $i < $_GET["cols"]; $i++){
            echo "<tr>";
            for($j=0; $j < $_GET["colspan"]; $j++){
              echo "<td>大島</td>";
            }
            echo "</tr>";
          }
          ?>
      </table>
    </body>
</html>
