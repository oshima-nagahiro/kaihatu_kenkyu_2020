<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content="width=device-width", initial-scale='1'>
        <title>既存社員情報修正</title>
        <script src="include/functions.js"></script>
    </head>
    <body>
        <?php
        include("./include/statics.php");
        // echo "<pre>";
        // var_dump($_GET);
        // echo "</pre>";
        $DB_DSN = "mysql:host=localhost; dbname=noshima; charset=utf8";
        $DB_USER = "webaccess";
        $DB_PW = "toMeu4rH";
        $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

        $query_str = "SELECT m.member_ID,m.name,m.pref,m.seibetu,m.age,m.grade_ID,m.section_ID
                        FROM member AS m
                        LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                        LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                        WHERE m.member_ID = " . $_GET['member_ID'] ;
        // echo $query_str;
        $sql = $pdo->prepare($query_str);
        $sql->execute();
        $result = $sql->fetchAll();
        // echo "<pre>";
        // var_dump($result);
        // echo "</pre>";
        ?>

        <table border="0" style="width:100%">
            <th align="left"><font size="5">
                社員名簿システム
            </th>
            <td align="right"><font size="2">
                ｜<a href="./index.php">トップ画面</a>｜<a href="./entry01.php">新規社員登録へ</a>｜
            </td>
        </table>
        <hr/>
        <form method='post' action='./entry_update02.php?member_ID=<?php echo $_GET['member_ID']; ?>' name=entryform>
        <table class="table1" border=1 style="border-collapse: collapse">
        <?php
        if(count($result) == 1){
        ?>
        <tr>
            <th>社員ID</th>
                <td><?php echo $result[0]['member_ID']; ?></td>
            </tr>
        <tr>
            <th>名前</th>
                <td><input type='text' name="namae" maxlength='30' value="<?php echo $result[0]['name']; ?>"></td>
            </tr>
          <tr>
            <th>出身地</th>
            <td>
                <select name="pref">
                    <option value="" selected>都道府県</option>
                    <?php
                     foreach($pref_array as $key => $value){
                        if ($result[0]['pref'] == $key){
                            echo "<option value='" . $key . "' selected>" . $value . "</option>";
                        }else{
                            echo "<option value='" . $key . "'>" . $value . "</option>";
                        }
                     }
                     ?>
                </select>
            </td>
          </tr>
          <tr>
            <th>性別</th>
            <td>
                <input type=radio name="seibetu" value="1" <?php if ($result[0]['seibetu']=="1") echo "checked";?>>男
                <input type=radio name="seibetu" value="0" <?php if ($result[0]['seibetu']=="0") echo "checked";?>>女
            </td>
          </tr>
          <tr>
            <th>年齢</th>
            <td>
                <input type='number' name="age" maxlength='2' value="<?php echo $result[0]['age']; ?>">才</td>
          </tr>
          <tr>
            <th>所属部署</th>
            <td>
                <input type=radio name="section_ID" value="1" <?php if ($result[0]['section_ID']=="1") echo "checked";?>>第一事業部
                <input type=radio name="section_ID" value="2" <?php if ($result[0]['section_ID']=="2") echo "checked";?>>第二事業部
                <input type=radio name="section_ID" value="3" <?php if ($result[0]['section_ID']=="3") echo "checked";?>>営業
                <input type=radio name="section_ID" value="4" <?php if ($result[0]['section_ID']=="4") echo "checked";?>>総務
                <input type=radio name="section_ID" value="5" <?php if ($result[0]['section_ID']=="5") echo "checked";?>>人事
            </td>
          </tr>
          <tr>
            <th>役職</th>
            <td>
                <input type=radio name="grade_ID" value="1" <?php if ($result[0]['grade_ID']=="1") echo "checked";?>>事業部長
                <input type=radio name="grade_ID" value="2" <?php if ($result[0]['grade_ID']=="2") echo "checked";?>>部長
                <input type=radio name="grade_ID" value="3" <?php if ($result[0]['grade_ID']=="3") echo "checked";?>>チームリーダー
                <input type=radio name="grade_ID" value="4" <?php if ($result[0]['grade_ID']=="4") echo "checked";?>>リーダー
                <input type=radio name="grade_ID" value="5" <?php if ($result[0]['grade_ID']=="5") echo "checked";?>>メンバー
            </td>
          </tr>
          <?php
        }
          ?>
        </table>
        <br/>
                <input type="button" value="登録" onclick="conf();">
                <input type="hidden" name="member_ID" value="<?php echo $result[0]['member_ID']; ?>">
                <input type="reset" value="リセット">
        </form>
    </body>
</html>
