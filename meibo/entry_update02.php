<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content="width=device-width", initial-scale='1'>
        <title>既存社員情報修正</title>
    </head>
    <body>
        <?php
        include("./include/statics.php");
        // echo "パラメータ確認<pre>";
        // var_dump($_POST);
        // echo "</pre>";

        // パラメータチェック開始
        $err = 0; // エラーフラグ
        // 社員IDのチェック
        if(isset($_POST['member_ID']) AND $_POST['member_ID'] != ""){
            $param_member_ID = $_POST['member_ID'];
        }else {
            $err = 1;
            echo "社員IDのチェックエラー";
        }

        // 名前のチェック
        if(isset($_POST['namae']) AND $_POST['namae'] != ""){
            $param_namae = $_POST['namae'];
        }else {
            $err = 1;
            echo "名前のチェックエラー";
        }

        // 都道府県コードのチェック 数字かどうかのチェックも入っております
        if(isset($_POST['pref']) AND $_POST['pref'] != "" AND is_numeric($_POST['pref'])){
            $param_pref = $_POST['pref'];
        }else {
            $err = 1;
            echo "都道府県コードのチェックエラー";
        }

        //性別のチェック
        if(isset($_POST['seibetu']) AND $_POST['seibetu'] != ""){
            $param_seibetu = $_POST['seibetu'];
        }else {
            $err = 1;
            echo "性別のチェックエラー";
        }

        //年齢のチェック
        if(isset($_POST['age']) AND $_POST['age'] != ""){
            $param_age = $_POST['age'];
        }else {
            $err = 1;
            echo "年齢コードのチェックエラー";
        }

        //所属部署のチェック
        if(isset($_POST['section_ID']) AND $_POST['section_ID'] != ""){
            $param_section_ID = $_POST['section_ID'];
        }else {
            $err = 1;
            echo "所属部署のチェックエラー";
        }

        //役職のチェック
        if(isset($_POST['grade_ID']) AND $_POST['grade_ID'] != ""){
            $param_grade_ID = $_POST['grade_ID'];
        }else {
            $err = 1;
            echo "役職のチェックエラー";
        }
        //下記の２種は同じ内容
        if($err == 0){
//        if(!$err){
            $DB_DSN = "mysql:host=localhost; dbname=noshima; charset=utf8";
            $DB_USER = "webaccess";
            $DB_PW = "toMeu4rH";
            $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
            $query_str = "UPDATE member
                          SET name = '" . $param_namae . "', pref = '" . $param_pref . "', seibetu = '" . $param_seibetu . "', age = '" . $param_age . "', section_ID = '". $param_section_ID . "' , grade_ID = '" . $param_grade_ID . "'
                          WHERE member.member_ID = " . $param_member_ID;
            // $query_str = "INSERT INTO member (member_ID,name,pref,seibetu,age,section_ID,grade_ID)
            //                VALUES (NULL,'" . $param_namae . "','" . $param_pref . "','" . $param_seibetu . "','" . $param_age . "','". $param_section_ID . "','" . $param_grade_ID . "')";
            echo $query_str;

            $stmt = $pdo->prepare($query_str);
            $stmt->execute();

            $url = 'detail01.php?member_ID=' . $param_member_ID;

             header('Location: ' . $url);

             exit;

        }else{
        ?>
            エラーが発生しました。<a href="./index.php">トップページ</a>に戻ってください。
        <?php
        }
        ?>
    </body>
</html>
