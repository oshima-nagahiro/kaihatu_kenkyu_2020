<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content="width=device-width", initial-scale='1'>
        <title>新規社員情報登録</title>
        <script src="include/functions.js"></script>
    </head>
    <body>
        <?php
        include("./include/statics.php");
        ?>
        <table border="0" style="width:100%">
            <th align="left"><font size="5">
                社員名簿システム
            </th>
            <td align="right"><font size="2">
                ｜<a href="./index.php">トップ画面</a>｜<a href="./entry01.php">新規社員登録へ</a>｜
            </td>
        </table>
        <hr/>
        <form method='post' action='entry02.php' name='entryform'>
        <table class="table1" border=1 style="border-collapse: collapse">
          <tr>
            <th>名前</th>
            <td><input type='text' name="namae" maxlength='30'></td>
          </tr>
          <tr>
            <th>出身地</th>
            <td><select name="pref">
                <option value="" selected>都道府県</option>
                <?php
                 foreach($pref_array as $key => $value){
                    echo "<option value='" . $key . "'>" . $value . "</option>";
                 }
                 ?>
                </select> </td>
          </tr>
          <tr>
            <th>性別</th>
            <td>
                <input type=radio name="seibetu" value="1" checked="checked">男
                <input type=radio name="seibetu" value="0">女
            </td>
          </tr>
          <tr>
            <th>年齢</th>
            <td><input type='number' name="age" maxlength='2'>才</td>
          </tr>
          <tr>
            <th>所属部署</th>
            <td>
                <input type=radio name="section_ID" value="1" checked="checked">第一事業部
                <input type=radio name="section_ID" value="2">第二事業部
                <input type=radio name="section_ID" value="3">営業
                <input type=radio name="section_ID" value="4">総務
                <input type=radio name="section_ID" value="5">人事
            </td>
          </tr>
          <tr>
            <th>役職</th>
            <td>
                <input type=radio name="grade_ID" value="1" checked="checked">事業部長
                <input type=radio name="grade_ID" value="2">部長
                <input type=radio name="grade_ID" value="3">チームリーダー
                <input type=radio name="grade_ID" value="4">リーダー
                <input type=radio name="grade_ID" value="5">メンバー
            </td>
          </tr>
        </table>
        <br/>
                <input type="button" value="登録" onclick="conf();">
                <input type="reset" value="リセット">
        </form>
    </body>
</html>
