<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content="width=device-width", initial-scale='1'>
        <title>社員名簿システム</title>
        <script>
            function resetform(){
                document.searchform.search_name.value="";
                document.searchform.seibetu.value="";
                document.searchform.section.value="";
                document.searchform.grade.value="";
            }
        </script>
    </head>
    <body>
        <?php
        // echo "<pre>";
        // var_dump($_GET);
        // echo "</pre>";
        $DB_DSN = "mysql:host=localhost; dbname=noshima; charset=utf8";
        $DB_USER = "webaccess";
        $DB_PW = "toMeu4rH";
        $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

        $query_str = "SELECT m.member_ID,m.name,m.pref,m.seibetu,m.age,gm.grade_name,sm.section_name
                        FROM member AS m
                        LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                        LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                        WHERE 1=1";

        if(isset($_GET['search_name']) AND $_GET['search_name'] != ""){
            $query_str .= " AND m.name LIKE '%" . $_GET['search_name'] . "%'";
        }
        if(isset($_GET['seibetu']) AND $_GET['seibetu'] != ""){
            $query_str .= " AND m.seibetu = " . $_GET['seibetu'];
        }
        if(isset($_GET['section']) AND $_GET['section'] != ""){
            $query_str .= " AND m.section_ID = " . $_GET['section'];
        }
        if(isset($_GET['grade']) AND $_GET['grade'] != ""){
            $query_str .= " AND m.grade_ID = " . $_GET['grade'];
        }
        // echo $query_str;
        $sql = $pdo->prepare($query_str);
        $sql->execute();
        $result = $sql->fetchAll();
        ?>
        <table border="0" style="width:100%">
            <th align="left"><font size="5">
                社員名簿システム
            </th>
            <td align="right"><font size="2">
                ｜<a href="./index.php">トップ画面</a>｜<a href="./entry01.php">新規社員登録へ</a>｜
            </td>
        </table>
        <hr/>
        <form method='get' action='index.php' name='searchform'>
            <b>名前:</b>
            <input type='text' name="search_name" value="<?php if(isset($_GET['search_name']) AND $_GET['search_name'] != "") echo $_GET['search_name']; ?>">
            <br/>
            <b>性別:</b>
             <select name="seibetu">
                 <option value="">すべて</option>
                 <option value="1" <?php echo array_key_exists('seibetu', $_GET) && $_GET['seibetu'] == '1' ? 'selected' : ''; ?>>男</option>
                 <option value="0" <?php echo array_key_exists('seibetu', $_GET) && $_GET['seibetu'] == '0' ? 'selected' : ''; ?>>女</option>
             </select>
             <b>部署:</b>
              <select name="section">
                  <option value="">すべて</option>
                  <option value="1" <?php echo array_key_exists('section', $_GET) && $_GET['section'] == '1' ? 'selected' : ''; ?>>第一事業部</option>
                  <option value="2" <?php echo array_key_exists('section', $_GET) && $_GET['section'] == '2' ? 'selected' : ''; ?>>第二事業部</option>
                  <option value="3" <?php echo array_key_exists('section', $_GET) && $_GET['section'] == '3' ? 'selected' : ''; ?>>営業</option>
                  <option value="4" <?php echo array_key_exists('section', $_GET) && $_GET['section'] == '4' ? 'selected' : ''; ?>>総務</option>
                  <option value="5" <?php echo array_key_exists('section', $_GET) && $_GET['section'] == '5' ? 'selected' : ''; ?>>人事</option>
              </select>
              <b>役職:</b>
               <select name="grade">
                   <option value="">すべて</option>
                   <option value="1" <?php echo array_key_exists('grade', $_GET) && $_GET['grade'] == '1' ? 'selected' : ''; ?>>事業部長</option>
                   <option value="2" <?php echo array_key_exists('grade', $_GET) && $_GET['grade'] == '2' ? 'selected' : ''; ?>>部長</option>
                   <option value="3" <?php echo array_key_exists('grade', $_GET) && $_GET['grade'] == '3' ? 'selected' : ''; ?>>チームリーダー</option>
                   <option value="4" <?php echo array_key_exists('grade', $_GET) && $_GET['grade'] == '4' ? 'selected' : ''; ?>>リーダー</option>
                   <option value="5" <?php echo array_key_exists('grade', $_GET) && $_GET['grade'] == '5' ? 'selected' : ''; ?>>メンバー</option>
               </select>
               <br/>
               <input type="submit" value="検索">
               <input type="button" value="リセット" onclick="resetform()">
        </form>
        <hr/>
        検索結果:
        <?php echo count($result); ?>
        <table border="1" style="border-collapse: collapse">
          <tr>
             <th>社員ID</th>
             <th>名前</th>
             <th>部署</th>
             <th>役職</th>
          </tr>

        <?php
        if(count($result) != 0){
            foreach($result as $each){
              echo "<tr>
                        <td>" . $each['member_ID'] . "</td>
                        <td><a href='./detail01.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>
                        <td>" . $each['section_name'] . "</td>
                        <td>" . $each['grade_name'] . "</td>
                    </tr>";
            }
        }else {
          echo
          "<tr>
            <td colspan='4'>検索結果なし</td>
          </tr>";
        }
        ?>
    </body>
</html>
