<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content="width=device-width", initial-scale='1'>
        <title>社員情報詳細</title>
        <script>
            function conf(){
                if(window.confirm('削除を行います。よろしいですか?')){
                    // alert('実行します');
                    document.entryform.submit();
                // }else{
                //     alert('中止します');
                }
            }
        </script>
    </head>
    <body>
        <?php
        include("./include/statics.php");
        // echo "<pre>";
        // var_dump($_GET);
        // echo "</pre>";
        $err = 0; // エラーフラグ
        // 社員IDのチェック
        if(isset($_GET['member_ID']) AND $_GET['member_ID'] != ""){
            $param_member_ID = $_GET['member_ID'];
        }else {
            $err = 1;
            $url = "./index.php";
            header('Location: ' . $url);
        }

        $DB_DSN = "mysql:host=localhost; dbname=noshima; charset=utf8";
        $DB_USER = "webaccess";
        $DB_PW = "toMeu4rH";
        $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

        $query_str = "SELECT m.member_ID,m.name,m.pref,m.seibetu,m.age,gm.grade_name,sm.section_name
                        FROM member AS m
                        LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                        LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                        WHERE m.member_ID = " . $_GET['member_ID'];
        // echo $query_str;
        $sql = $pdo->prepare($query_str);
        $sql->execute();
        $result = $sql->fetchAll();
        // echo "<pre>";
        // var_dump($result);
        // echo "</pre>";
        ?>

        <table border="0" style="width:100%">
            <th align="left"><font size="5">
                社員名簿システム
            </th>
            <td align="right"><font size="2">
                ｜<a href="./index.php">トップ画面</a>｜<a href="./entry01.php">新規社員登録へ</a>｜
            </td>
        </table>
        <hr/>
        <table class="table1" border=1 style="border-collapse: collapse">
          <?php
          if(count($result) == 1){
          ?>
          <tr>
            <th>社員ID</th>
            <td><?php echo $result[0]['member_ID'] ?></td>
          </tr>
          <tr>
            <th>名前</th>
            <td><?php echo $result[0]['name']; ?></td>
          <tr>
          </tr>
            <th>出身地</th>
            <td><?php echo $pref_array[$result[0]['pref']] ?></td>
          </tr>
          <tr>
            <th>性別</th>
            <td><?php echo $gender_array[$result[0]['seibetu']] ?></td>
          </tr>
          <tr>
            <th>年齢</th>
            <td><?php echo $result[0]['age'] ?></td>
          </tr>
          <tr>
            <th>所属部署</th>
            <td><?php echo $result[0]['section_name'] ?></td>
          </tr>
          <tr>
            <th>役職</th>
            <td><?php echo $result[0]['grade_name'] ?></td>
          </tr>
          <!-- // }else {
          //   echo -->
          <?php
          }
          ?>
      </table>
      <br/>
      <form method='post' action='./entry_update01.php?member_ID=<?php echo $_GET['member_ID']; ?>'>
          <input type="submit" value="編集">
      </form>
      <form method='post' action='./delete.php'>
          <input type="submit" value="削除" onclick="conf();">
          <input type="hidden" name="member_ID" value="<?php echo $result[0]['member_ID']; ?>">
      </form>
  </body>
</html>
