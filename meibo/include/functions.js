function conf(){
    if(document.entryform.namae.value == ""){
        alert('名前は必須です');
        return false;
    }
    if(document.entryform.pref.value == ""){
        alert('都道府県は必須です');
        return false;
    }
    var age_temp = document.entryform.age.value;
    if(age_temp == "" || age_temp < 1 || age_temp > 99){
        alert('年齢は必須です/数値を入力してください/1-99の範囲で入力してください');
        return false;
    }
    if(window.confirm('更新を行います。よろしいですか?')){
        // alert('実行します');
        document.entryform.submit();
    // }else{
    //     alert('中止します');
    }
}
