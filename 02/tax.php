<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width", initial-scale="1">
    <title>第二回課題、消費税計算ページ</title>
  </head>
  <body>
      <h1>第二回課題、消費税計算ページ</h1>
        <form method="POST" action="tax.php">
          <table border="1" style="border-collapse: collapse">
            <tr>
              <th>
                  商品名
              </th>
              <th>
                  価格(単位:円、税抜き)
              </th>
              <th>
                  個数
              </th>
              <th>
                  税率
              </th>
            </tr>
            <tr>
              <td>
                <input type='text' name="syohinmei01">
              </td>
              <td>
                <input type='text' name="kakaku01">
              </td>
              <td>
                <input type='text' name="kosu01">個
              </td>
              <td>
                <input type="radio" name="tax_rate01" value="0.08" checked>8%
                <input type="radio" name="tax_rate01" value="0.10">10%
            </tr>
            <tr>
              <td>
                <input type='text' name="syohinmei02">
              </td>
              <td>
                <input type='text' name="kakaku02">
              </td>
              <td>
                <input type='text' name="kosu02">個
              </td>
              <td>
                <input type="radio" name="tax_rate02" value="0.08" checked>8%
                <input type="radio" name="tax_rate02" value="0.10">10%
            </tr>
            <tr>
              <td>
                <input type='text' name="syohinmei03">
              </td>
              <td>
                <input type='text' name="kakaku03">
              </td>
              <td>
                <input type='text' name="kosu03">個
              </td>
              <td>
                <input type="radio" name="tax_rate03" value="0.08" checked>8%
                <input type="radio" name="tax_rate03" value="0.10">10%
            </tr>
            <tr>
              <td>
                <input type='text' name="syohinmei04">
              </td>
              <td>
                <input type='text' name="kakaku04">
              </td>
              <td>
                <input type='text' name="kosu04">個
              </td>
              <td>
                <input type="radio" name="tax_rate04" value="0.08" checked>8%
                <input type="radio" name="tax_rate04" value="0.10">10%
            </tr>
            <tr>
              <td>
                <input type='text' name="syohinmei05">
              </td>
              <td>
                <input type='text' name="kakaku05">
              </td>
              <td>
                <input type='text' name="kosu05">個
              </td>
              <td>
                <input type="radio" name="tax_rate05" value="0.08" checked>8%
                <input type="radio" name="tax_rate05" value="0.10">10%
            </tr>
          </table>
          <input type="submit" value="送信">
          <input type="reset" value="リセット">
        </form>
        <hr>
        <table border="1" style="border-collapse: collapse">
          <tr>
            <th>
                商品名
            </th>
            <th>
                価格(単位:円、税抜き)
            </th>
            <th>
                個数
            </th>
            <th>
                税率
            </th>
            <th>
                小計(単位:円)
            </th>
          </tr>
          <tr>
            <td>
              <?php
                echo $_POST["syohinmei01"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kakaku01"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kosu01"];
                ?>
            </td>
            <td>
              <?php
                echo $_POST["tax_rate01"];
              ?>
            </td>
            <td>
              <?php
                echo $total01= $_POST["kakaku01"]*$_POST["kosu01"]+floor($_POST["kakaku01"]*$_POST["kosu01"]*$_POST["tax_rate01"]);
              ?>
            </td>
          </tr>
          <tr>
            <td>
              <?php
                echo $_POST["syohinmei02"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kakaku02"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kosu02"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["tax_rate02"];
              ?>
            </td>
            <td>
              <?php
                echo $total02= $_POST["kakaku02"]*$_POST["kosu02"]+floor($_POST["kakaku02"]*$_POST["kosu02"]*$_POST["tax_rate02"]);
              ?>
            </td>
          </tr>
          <tr>
            <td>
              <?php
                echo $_POST["syohinmei03"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kakaku03"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kosu03"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["tax_rate03"];
              ?>
            </td>
            <td>
              <?php
                echo $total03= $_POST["kakaku03"]*$_POST["kosu03"]+floor($_POST["kakaku03"]*$_POST["kosu03"]*$_POST["tax_rate03"]);
              ?>
            </td>
          </tr>
          <tr>
            <td>
              <?php
                echo $_POST["syohinmei04"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kakaku04"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kosu04"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["tax_rate04"];
              ?>
            </td>
            <td>
              <?php
                echo $total04= $_POST["kakaku04"]*$_POST["kosu04"]+floor($_POST["kakaku04"]*$_POST["kosu04"]*$_POST["tax_rate04"]);
              ?>
            </td>
          </tr>
          <tr>
            <td>
              <?php
                echo $_POST["syohinmei05"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kakaku05"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["kosu05"];
              ?>
            </td>
            <td>
              <?php
                echo $_POST["tax_rate05"];
              ?>
            </td>
            <td>
              <?php
                echo $total05= $_POST["kakaku05"]*$_POST["kosu05"]+floor($_POST["kakaku05"]*$_POST["kosu05"]*$_POST["tax_rate05"]);
              ?>
            </td>
          </tr>
          <tr>
            <td  colspan="4">
              合計
            </td>
            <td>
              <?php
                echo $total01+$total02+$total03+$total04+$total05;
              ?>
            </td>
          </tr>
        </table>
  </body>
</html>
